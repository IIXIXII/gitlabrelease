#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -----------------------------------------------------------------------------
#
# MIT License
#
# Copyright (c) 2018 Florent TOURNOIS
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# -----------------------------------------------------------------------------

import logging
# import gitlab

# -----------------------------------------------------------------------------
def get_the_one(elements):
    """ Get the one element in the list
    """
    if len(elements) < 1:
        logging.error("No element found.")
        raise IndexError("No element found.")

    if len(elements) > 1:
        logging.error("Too many elements found.")
        raise IndexError("Too many elements found.")

    return elements[0]

# -----------------------------------------------------------------------------
def find_project(git, **kwargs):
    logging.info("Search projects with %s", kwargs)

    # with the id
    if 'id' in kwargs:
        return git.projects.get(kwargs['id'])

    # with all projects
    all_projects = git.projects

    if "group" in kwargs:
        group = get_the_one(git.groups.list(search=kwargs["group"]))
        all_projects = group.projects
        del kwargs["group"]

    return get_the_one(all_projects.list(**kwargs))
