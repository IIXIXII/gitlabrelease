#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -----------------------------------------------------------------------------
#
# MIT License
#
# Copyright (c) 2018 Florent TOURNOIS
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# -----------------------------------------------------------------------------

import sys


# -----------------------------------------------------------------------------
# NullStream behaves like a stream but does nothing.
# -----------------------------------------------------------------------------
class NullStream:
    def __init__(self):
        pass

    def write(self, data):
        pass

    def read(self, data):
        pass

    def flush(self):
        pass

    def close(self):
        pass


# -----------------------------------------------------------------------------
# Storage for stream
# -----------------------------------------------------------------------------
class InOutStream:
    def __init__(self, stream=NullStream()):
        self.stdout = stream
        self.stderr = stream
        self.stdin = stream
        self.__stdout__ = stream
        self.__stderr__ = stream
        self.__stdin__ = stream

    def save_current_stream(self):
        self.stdout = sys.stdout
        self.stderr = sys.stderr
        self.stdin = sys.stdin
        self.__stdout__ = sys.__stdout__
        self.__stderr__ = sys.__stderr__
        self.__stdin__ = sys.__stdin__

    def apply_to_std_stream(self):
        sys.stdout = self.stdout
        sys.stderr = self.stderr
        sys.stdin = self.stdin
        sys.__stdout__ = self.__stdout__
        sys.__stderr__ = self.__stderr__
        sys.__stdin__ = self.__stdin__


# -----------------------------------------------------------------------------
# define a static decorator for function
#
# @code{.py}
# 	(at)static(__folder_md_test__=None)
# 	def get_test_folder():
#     		if get_test_folder.__folder_md_test__ is None:
#         	get_test_folder.__folder_md_test__ = check_folder(os.path.join(
#             		os.path.split(__get_this_filename())[0], "test-md"))
#     		return get_test_folder.__folder_md_test__
# @endcode
#
# @param kwargs list of arguments
# @return the wrap function
# -----------------------------------------------------------------------------
def static(**kwargs):
    def wrap(the_decorated_function):
        for key, value in kwargs.items():
            setattr(the_decorated_function, key, value)
        return the_decorated_function
    return wrap


# -----------------------------------------------------------------------------
# Retrieve the initial stream
# -----------------------------------------------------------------------------
@static(__stream__=None)
def initial_stream():
    if initial_stream.__stream__ is None:
        initial_stream.__stream__ = InOutStream()
        initial_stream.__stream__.save_current_stream()
    return initial_stream.__stream__


# -----------------------------------------------------------------------------
# Test the frozen situation of the executable
# -----------------------------------------------------------------------------
def is_frozen():
    return getattr(sys, 'frozen', False)


# -----------------------------------------------------------------------------
# Change the std stream in the frozen case
# -----------------------------------------------------------------------------
if is_frozen():
    initial_stream().save_current_stream()
    # InOutStream(NullStream()).apply_to_std_stream()
